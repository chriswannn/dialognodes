﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;


[CreateAssetMenu(fileName = "new character dialogs graph", menuName = "Character Dialogs Graph")]
public class DialogCharacter : NodeGraph
{
        public string charactername;
}
