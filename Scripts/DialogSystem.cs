using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogSystem : MonoBehaviour
{
    public TMP_Text customeranswer_text;
    public List<Button> answersbutton;
    public List<TMP_Text> answersbutton_text;
    public DialogCharacter activegraph;
    public GameObject dialogpanel;
    public Dialogobject _activedialog;


    private void Start()
    {
        GetPanel();
        foreach (Dialogobject node in activegraph.nodes)
        {
            if (!node.GetInputPort("input").IsConnected)
            {
                UpdateDialog(node);
                break;
            }
        }
        
        
    }

    void GetPanel()
    {
        answersbutton_text = new List<TMP_Text>(answersbutton.Count);
        foreach (Button button in answersbutton)
        {
            answersbutton_text.Add(button.transform.GetChild(0).GetComponent<TMP_Text>());
        }
    }

    public void GetNewDialog(int index)
    {
        if (index < _activedialog.answers.Count)
        {
            var port = _activedialog.GetPort("answers "+index);
            if (port.IsConnected)
            {
                UpdateDialog(port.Connection.node as Dialogobject);
            }
            else
            {
                dialogend();
            }
        }
        else
        {
            Debug.LogError("Trying to access to an unknown answer");
        }
    }

    private async Task dialogend()
    {
        await Task.Delay(2000);
        dialogpanel.SetActive(false);
    }



    void UpdateDialog(Dialogobject dialog)
    {
        _activedialog = dialog;
        customeranswer_text.text = dialog.customeranswer;
        for (int i = 0; i < answersbutton.Count; i++)
        {
            if (i < dialog.answers.Count)
            {
                answersbutton_text[i].text = dialog.answers[i];
                answersbutton[i].interactable = true;
            }
            else
            {
                answersbutton_text[i].text = "";
                answersbutton[i].interactable = false;
            }
            
        }
    }
}
