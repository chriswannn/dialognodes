using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using XNode;
using XNodeEditor;


[CustomNodeEditor(typeof(Dialogobject))]
public class DialogNodeEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        var node = serializedObject.targetObject as Dialogobject;
        NodeEditorGUILayout.PortField(node.GetPort("input"));
        
        GUILayout.Label("Customer Answer");
        node.customeranswer = GUILayout.TextArea(node.customeranswer, new GUILayoutOption[]
        {
            GUILayout.MinHeight(50)
        });

        NodeEditorGUILayout.DynamicPortList(
            "answers",
            typeof(string),
            serializedObject,
            NodePort.IO.Input,
            Node.ConnectionType.Override,
            Node.TypeConstraint.None,
            OnCreateReorderableList
            );

        foreach (NodePort dynamicPort in target.DynamicPorts)
        {
            if (NodeEditorGUILayout.IsDynamicPortListPort(dynamicPort)) continue;
            {
                NodeEditorGUILayout.PortField(dynamicPort);
            }
        }
        
        
        
        
        
        serializedObject.ApplyModifiedProperties();
    }
    
    void OnCreateReorderableList(ReorderableList list)
    {
        list.elementHeightCallback = (int index) =>
        {
            return 60;
        };
            
        // Override drawHeaderCallback to display node's name instead
        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var segment = serializedObject.targetObject as Dialogobject;

            NodePort port = segment.GetPort( "answers " + index);

            segment.answers[index] = GUI.TextArea(rect, segment.answers[index]);

            if (port != null) {
                Vector2 pos = rect.position + (port.IsOutput?new Vector2(rect.width + 6, 0) : new Vector2(-36, 0));
                NodeEditorGUILayout.PortField(pos, port);
            }
        };
    }
}
