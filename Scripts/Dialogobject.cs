using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;


[Serializable]
public struct Connection {}

public class Dialogobject : Node
{


    [Input]
    public Connection input;
    
    public string customeranswer;
    public int satisfaction = 50;
    [Output(dynamicPortList = true)]
    public List<string> answers;


    public override object GetValue(NodePort port)
    {
        return base.GetValue(port);
    }

}
